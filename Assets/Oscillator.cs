﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class Oscillator : MonoBehaviour
{
    [SerializeField] Vector3 movementVector = new Vector3(10f, 10f,10f);
    [SerializeField] float period = 2f;

    float movementFactor;
    Vector3 startingPos;

    // Start is called before the first frame update
    void Start()
    {
        startingPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        //set movementr acor auto
        // todo protect against divide by zero error
        float cycles = Time.time / period; //grows continually from 0
        if (period <= Mathf.Epsilon) { return; }
  
        const float tau = Mathf.PI * 2f; //about 6.28
        float rawSineWave = Mathf.Sin(cycles * tau);

        movementFactor = rawSineWave / 2f + 0.5f;

        print(rawSineWave);

        Vector3 offset = movementVector * movementFactor;
        transform.position = startingPos+offset;
    }
}
